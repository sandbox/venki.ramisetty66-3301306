<?php

namespace Drupal\text_assets\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
/**
 * Returns responses for Text assets routes.
 */
class TextAssetsController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build($text_assets) {
    $c_type='text/plain';
    $type = $text_assets->get('asset_type')->getString();
    if($type != null)
    {
      $c_type = str_replace('_','/',$type);
    }
    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('text_assets');
    $pre_render = $view_builder->view($text_assets);
    $output = \Drupal::service('renderer')->renderRoot($pre_render);
    $response = new Response();
    $response->headers->set('Content-Type', $c_type);
    $response->setContent($output);

    return $response;
  }

}