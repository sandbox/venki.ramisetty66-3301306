<?php

namespace Drupal\text_assets;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a text_assets entity type.
 */
interface TextAssetsInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the text_assets title.
   *
   * @return string
   *   Title of the text_assets.
   */
  public function getTitle();

  /**
   * Sets the text_assets title.
   *
   * @param string $title
   *   The text_assets title.
   *
   * @return \Drupal\text_assets\TextAssetsInterface
   *   The called text_assets entity.
   */
  public function setTitle($title);

  /**
   * Gets the text_assets creation timestamp.
   *
   * @return int
   *   Creation timestamp of the text_assets.
   */
  public function getCreatedTime();

  /**
   * Sets the text_assets creation timestamp.
   *
   * @param int $timestamp
   *   The text_assets creation timestamp.
   *
   * @return \Drupal\text_assets\TextAssetsInterface
   *   The called text_assets entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the text_assets status.
   *
   * @return bool
   *   TRUE if the text_assets is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the text_assets status.
   *
   * @param bool $status
   *   TRUE to enable this text_assets, FALSE to disable.
   *
   * @return \Drupal\text_assets\TextAssetsInterface
   *   The called text_assets entity.
   */
  public function setStatus($status);

}
