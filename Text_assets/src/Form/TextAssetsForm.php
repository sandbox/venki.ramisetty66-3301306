<?php

namespace Drupal\text_assets\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the text_assets entity edit forms.
 */
class TextAssetsForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New text_assets %label has been created.', $message_arguments));
      $this->logger('text_assets')->notice('Created new text_assets %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The text_assets %label has been updated.', $message_arguments));
      $this->logger('text_assets')->notice('Updated new text_assets %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.text_assets.canonical', ['text_assets' => $entity->id()]);
  }

}
